<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\Books\BookRequest;
use App\Models\Book;
use App\Models\Tag;
use Illuminate\Http\Request;

class BookController extends BaseController
{

    protected $book = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Book $book)
    {
        $this->middleware('auth:api');
        $this->book = $book;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = $this->book->latest()->with('status')->paginate(10);

        return $this->sendResponse($books, 'Book list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Books\BookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $book = $this->book->create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'status_id' => $request->get('status_id'),
        ]);

        return $this->sendResponse($book, 'Book Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = $this->book->with(['status', 'tags'])->findOrFail($id);

        return $this->sendResponse($book, 'Book Details');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        $book = $this->book->findOrFail($id);

        $book->update($request->all());

        return $this->sendResponse($book, 'Book Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $book = $this->book->findOrFail($id);

        $book->delete();

        return $this->sendResponse($book, 'Book has been Deleted');
    }

    public function upload(Request $request)
    {
        $fileName = time() . '.' . $request->file->getClientOriginalExtension();
        $request->file->move(public_path('upload'), $fileName);

        return response()->json(['success' => true]);
    }
}
