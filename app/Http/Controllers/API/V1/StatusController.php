<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends BaseController
{
    protected $status = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Status $status)
    {
        $this->middleware('auth:api');
        $this->status = $status;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = $this->status->latest()->paginate(10);

        return $this->sendResponse($statuses, 'Status list');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $status = $this->status->pluck('name', 'id');

        return $this->sendResponse($status, 'Status list');
    }


    /**
     * Store a newly created resource in storage.
     *
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $tag = $this->status->create([
            'name' => $request->get('name')
        ]);

        return $this->sendResponse($tag, 'Status Created Successfully');
    }

    /**
     * Update the resource in storage
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $tag = $this->status->findOrFail($id);

        $tag->update($request->all());

        return $this->sendResponse($tag, 'Status Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $status = $this->status->findOrFail($id);

        $status->delete();

        return $this->sendResponse($status, 'Status has been Deleted');
    }
}
