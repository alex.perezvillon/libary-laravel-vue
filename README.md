
# Laravel+Vue Crud Library

## Tech Specification

- Laravel 10
- Vue 3 + VueRouter + vue-progressbar + sweetalert2 + laravel-vue-pagination
- Admin LTE 3 + Bootstrap 4 + Font Awesome 5
- PHPUnit Test

## Features

- Modal based Create+Edit, List with Pagination, Delete with Sweetalert
- Login, Register
- Avatar
- Book Management 
- User Management
- Build with Docker


## Install with Docker

- `docker-compose up -d`
- `docker exec -it vue-app-library /bin/bash`
- `cp .env.example .env`
- `php artisan key:generate`
- `php artisan db:seed`
- `php artisan passport:install`
- Application http://localhost:8008/
- Adminer for Database http://localhost:8282/
- DBhost: yourIP:3307, user: root, Password: 123456


## Unit Test

#### run PHPUnit

```bash
# run PHPUnit all test cases
vendor/bin/phpunit
```
