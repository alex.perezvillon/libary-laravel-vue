<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('books')->insert(
            [
                [
                    'name' => 'Clean Code',
                    'description' => 'Clean Code: A Handbook of Agile Software Craftsmanship (Robert C. Martin Series) Tapa blanda – Ilustrado, 1 agosto 2008. Edición en Inglés de Martin Robert.',
                    'status_id' => 4,
                ],
                [
                    'name' => 'DDD',
                    'description' => 'Domain-Driven Design(DDD) is a collection of principles and patterns that help developers craft elegant object systems. Properly applied it can lead to software abstractions called domain models. These models encapsulate complex business logic, closing the gap between business reality and code',
                    'status_id' => 2,
                ],
                [
                    'name' => 'Design Patterns',
                    'description' => 'In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem in software design.',
                    'status_id' => 2,
                ]
            ]
        );
    }
}
