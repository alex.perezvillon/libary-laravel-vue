<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->truncate();

        DB::table('statuses')->insert(
            [
                [
                    'name' => 'Delivered'
                ],
                [
                    'name' => 'Pending'
                ],
                [
                    'name' => 'In Stock'
                ],
                [
                    'name' => 'Reserved'
                ],
            ]
        );
    }
}
