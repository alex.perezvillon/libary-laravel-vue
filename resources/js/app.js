//
// import moment from 'moment';
//
import { Form, HasError, AlertError } from 'vform';
import Swal from 'sweetalert2';
import { createApp } from 'vue'
import Welcome from './components/Welcome'
import Books from './components/Books';
import Status from './components/Status';
import Users from './components/Users';
import VueProgressBar from "@aacassandra/vue3-progressbar";
import router from './routes';

import Gate from "./Gate";

window.Form = Form;

//Alert dynamic
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

window.Swal = Swal;
window.Toast = Toast;

require('./bootstrap')

const options = {
    color: "#bffaf3",
    failedColor: "#874b4b",
    thickness: "5px",
    transition: {
        speed: "0.2s",
        opacity: "0.6s",
        termination: 300,
    },
    autoRevert: true,
    location: "left",
    inverse: false,
};

const app = createApp({
    el: '#app',
    router
})
app.config.globalProperties.$gate = new Gate(window.user);

//Pages
app.component('welcome', Welcome);
app.component('books', Books);
app.component('status', Status);
app.component('users', Users);

//Libraries
app.use(router).use(VueProgressBar, options);

app.mount('#app');
