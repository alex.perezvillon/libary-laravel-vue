import { createRouter, createWebHistory } from "vue-router";

import Books from "./components/Books";
import Users from "./components/Users";
import Status from "./components/Status";

const routes = [
    {
        path: '/books',
        name: 'books',
        component: Books
    },
    {
        path: '/users',
        name: 'users',
        component: Users
    },
    {
        path: '/status',
        name: 'status',
        component: Status
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
